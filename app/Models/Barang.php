<?php

namespace App\Models;

use CodeIgniter\Model;
use Config\Database;

class Barang extends Model
{
	
	protected $DBGroup              = 'default';
	protected $table                = 'barang';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = false;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['id','nama','id_cabang'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	// public function __construct()
	// {
	// 	$this->db = Database::connect();	
	// }
	public function cabang()
	{
		return $this->select('barang.*,cabang.nama AS nama_cabang')->join('cabang','cabang.id = barang.id_cabang');
	}
}
