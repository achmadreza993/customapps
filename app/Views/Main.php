<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <!-- Css -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- Font & icon -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@500&family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">

    <title>Document</title>
</head>

<!-- basic css -->
<style>
    .DMSans {
        font-family: "DM Sans";
    }

    .openSans {
        font-family: "Open Sans";
    }

    .card {
        background-color: #FAFAF9;
        border: none;
    }

    .color1 {
        color: #2563EB;
    }

    .color2 {
        color: #94A3B8;
    }

    .color3 {
        color: #64748B;
    }

    .color4 {
        color: #FEFEFE;
    }

    .color5 {
        color: #EF4444;
    }

    .color6 {
        color: #FACC15;
    }

    .color7 {
        color: #22C55E;
    }

    .color8 {
        color: #0EA5E9;
    }

    .color9 {
        color: #3B82F6;
    }

    .color10 {
        color: #2DD4BF;
    }

    .color11 {
        color: #6366F1;
    }

    .color12 {
        color: #16A34A;
    }

    .color13 {
        color: #10B981;
    }

    .border1 {
        border-radius: .25rem;
        border: 2px solid #2563EB;
    }

    .border2 {
        border-radius: .25rem;
        border: 2px solid #94A3B8;
    }

    .border3 {
        border-radius: .25rem;
        border: 2px solid #64748B;
    }

    .border4 {
        border-radius: .25rem;
        border: 2px solid #FEFEFE;
    }

    .border5 {
        border-radius: .25rem;
        border: 2px solid #EF4444;
    }

    .border6 {
        border-radius: .25rem;
        border: 2px solid #FACC15;
    }

    .border7 {
        border-radius: .25rem;
        border: 2px solid #22C55E;
    }

    .border8 {
        border-radius: .25rem;
        border: 2px solid #0EA5E9;
    }

    .border9 {
        border-radius: .25rem;
        border: 2px solid #3B82F6;
    }

    .border10 {
        border-radius: .25rem;
        border: 2px solid #2DD4BF;
    }

    .border11 {
        border-radius: .25rem;
        border: 2px solid #6366F1;
    }

    .border12 {
        border-radius: .25rem;
        border: 2px solid #16A34A;
    }

    .border13 {
        border-radius: .25r3m;
        border: 2px solid #10B981;
    }

    .bg0 {
        background-color: transparent;
    }

    .bg1 {
        background-color: #2563EB;
    }

    .bg2 {
        background-color: #FAFAF9;
    }

    .bg3 {
        background-color: #F3F4F6;
    }

    .bg4 {
        background-color: #D1D5DB;
    }

    .bg5 {
        background-color: #EF4444;
    }

    .bg6 {
        background-color: #FACC15;
    }

    .bg7 {
        background-color: #22C55E;
    }

    .bg8 {
        background-color: #0EA5E9;
    }

    .bg9 {
        background-color: #3B82F6;
    }

    .bg10 {
        background-color: #2DD4BF;
    }

    .bg11 {
        background-color: #6366F1;
    }

    .bg12 {
        background-color: #16A34A;
    }

    .bg13 {
        background-color: #10B981;
    }

    .myBtn {
        border: none;
        border-radius: .25rem;
        outline: none;
    }

    .myBtn:focus {
        outline: none;
    }

    .myRounded {
        border: none;
        border-radius: .25rem;
        outline: none;
    }

    .myCard {
        border: none;
        border-radius: .25rem;
        padding: 2rem;
        outline: none;
    }

    ::-webkit-scrollbar {
        width: 8px;
        height: 8px;
    }

    ::-webkit-scrollbar-track {
        background: #EFF6FF;
    }

    ::-webkit-scrollbar-thumb {
        background: #94A3B8;
    }

    ::-webkit-scrollbar-thumb:hover {
        background: #888;
    }

    .sideBar {
        position: fixed;
    }

    .roundedTop {
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem;
    }
</style>
<!-- responsive css sm -->
<style>
    @media only screen and (max-width: 767px) {
        .sideBar {
            width: 0px;
            top: 0px;
            left: 0px;
            height: 100vh;
            background-color: #EFF6FF;
        }

        #reasonWrapper {
            height: 278px;
        }

        #backdrop,
        #backdrop1 {
            z-index: 20;
        }

        #topNav {
            z-index: 10;
        }

        #sideWrapper {
            z-index: 25;
            position: fixed;
            right: 0px;
            top: 2rem;
        }

        #sideContent {
            right: .6rem;
        }

        #usernameIpWrapper {
            display: block;
            justify-content: start;
        }

        #usernameIpWrapper p:first-child {
            display: block;
            margin-bottom: 1rem;
        }

        #usernameIpWrapper p {
            width: auto;
        }

        .todayInfoNav {
            display: block;
            width: 100%;
        }

        .employeeSideContent {
            height: 90vh;
        }



    }
</style>
<!-- responsive css md -->
<style>
    @media (min-width: 768px) and (max-width: 991px) {
        .sidebarItem p {
            justify-content: center;
        }

        #reasonWrapper {
            height: 285px;
        }

        #backdrop,
        #backdrop1 {
            z-index: 10;
        }

        .sideBar {
            width: 81px;
            margin-top: 1rem;
            background-color: #FAFAF9;
            box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15);
            border-radius: .25rem;
        }

        #topNav {
            z-index: 20;
        }


        #sideWrapper {
            z-index: 25;
            position: fixed;
            right: 0px;
        }

        #sideContent {
            right: 1rem;
            top: 5.5rem;
        }

        #employeeWrapper {
            margin-left: 2.9rem;
        }

        .employeeSideContent {
            height: min(80vh, 636px);
        }

        #usernameIpWrapper {
            display: flex;
            justify-content: space-between;
        }

        #usernameIpWrapper p:first-child {
            margin: 0;
        }

    }
</style>
<!-- responsive css xl -->
<style>
    @media only screen and (min-width: 992px) {
        .sideBar {
            width: auto;
            margin-top: 1rem;
            background-color: #FAFAF9;
            box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15);
            border-radius: .25rem;
        }

        #reasonWrapper {
            height: 285px;
        }

        .myTable {
            table-layout: fixed;
        }

        .employeeSideContent {
            height: auto;
        }

        #usernameIpWrapper {
            display: flex;
            justify-content: space-between;
        }

        #usernameIpWrapper p:first-child {
            margin: 0;
        }
    }
</style>



<body style="background-color: #EFF6FF;">
    <div class="d-flex justify-content-center" style="padding: 0; width: 100vw;position: fixed; z-index: 30;top: 2.5rem">
        <p id="notification" style="display: none;" class="color4 openSans m-0 myCard py-2 px-3 align-items-center">
            <span id="notificationMsg">Data berhasil ditambahkan</span>
        </p>
    </div>
    <!-- < md navbar  -->
    <div class="d-block d-lg-none pb-5">
        <div id="topNav" style="background-color: #EFF6FF;position: fixed;">
            <div class=" px-3 pt-2 d-flex justify-content-between align-items-center" style="width: 100vw;">
                <div class="d-flex align-items-center">
                    <button class="myBtn bg0 color1" id="sideBarBtn" onclick="showSideBar(true)"><i class="bi bi-list" style="font-size: 30px;"></i></button>
                    <h3 class="color1 mb-0 DMSans" id="navLogo">Logo</h3>
                </div>
                <h5 class="DMSans mb-1 color2 mr-3 d-none d-md-block">Achmad Reza Fahlevi</h5>
                <h5 class="color2 mb-0 DMSans mr-2 d-md-none">Beranda</h5>
            </div>
        </div>
    </div>
    <div class="row m-0 py-2 py-md-1 py-lg-4 py-xl-4 px-2 px-md-3 px-lg-4 px-xl-4 ">
        <!-- > md sidebar  -->
        <div class=" d-none d-md-block d-lg-block d-xl-block col-2 col-md-1 col-lg-2"></div>
        <div id="backdrop" class="position-fixed" style="left: 0px;display: none;top:0; width: 100vw; height: 100vh;background-color: rgb(17,24,39,.5);"></div>
        <div class="sideBar col-md-3 col-lg-2 p-0" style="z-index:30">
            <div class="row ml-lg-2 px-3 px-md-2 pl-lg-1 m-0 pt-lg-3">
                <nav id="sideBarContent" style="width: 100%;" class="d-none d-md-block d-lg-block d-xl-block col p-0">
                    <div class="pt-2 pb-3 d-md-none">
                        <button class="myBtn bg0 float-right" onclick="showSideBar(false)"><i class="bi bi-x color5" style="font-size: 30px;"></i></button>
                    </div>
                    <div class="p-3 d-md-none d-lg-block">
                        <h2 class="DMSans m-0 color1">LOGO</h2>
                        <p class="openSans m-0 color2">Achmad Reza Fahlevi</p>
                    </div>
                    <div class="pb-2" style="width: 100%;height: 75vh; overflow-y: auto;">
                        <div class="sidebarNav " style="width: 100%;">
                            <a href="/" class="text-decoration-none" style="width: 100%;">
                                <div class="px-3 my-3 sidebarItem <?php if ($status == "Home") echo "card shadow py-2 bg1" ?>">
                                    <p class="openSans m-0 d-flex align-items-center <?php if ($status == "Home") echo "color4";
                                                                                        else echo "color1" ?>"> <i class="bi bi-house" style="font-size: 25px;"></i><span class="sideNavText d-md-none d-lg-block"> &nbsp; Beranda</span></p>
                                </div>
                            </a>
                            <a href="/Barang" class="text-decoration-none" style="width: 100%;">
                                <div class="px-3 my-3 sidebarItem <?php if ($status == "Item") echo "card shadow py-2 bg9" ?>">
                                    <p class="openSans m-0 d-flex align-items-center <?php if ($status == "Item") echo "color4";
                                                                                        else echo "color9" ?>"> <i class="bi bi-archive" style="font-size: 25px;"></i><span class="sideNavText d-md-none d-lg-block"> &nbsp; Barang</span></p>
                                </div>
                            </a>
                            <a href="/Cabang" class="text-decoration-none" style="width: 100%;">
                                <div class="px-3 my-3 sidebarItem <?php if ($status == "Branch") echo "card shadow py-2 bg10" ?>">
                                    <p class="openSans m-0 d-flex align-items-center <?php if ($status == "Branch") echo "color4";
                                                                                        else echo "color10" ?>"> <i class="bi bi-diagram-3" style="font-size: 25px;"></i><span class="sideNavText d-md-none d-lg-block"> &nbsp; Cabang</span></p>
                                </div>
                            </a>
                            <a href="/Pegawai" class="text-decoration-none" style="width: 100%;">
                                <div class="px-3 my-3 sidebarItem <?php if ($status == "Employee") echo "card shadow py-2 bg11" ?>">
                                    <p class="openSans m-0 d-flex align-items-center <?php if ($status == "Employee") echo "color4";
                                                                                        else echo "color11" ?>"> <i class="bi bi-people" style="font-size: 25px;"></i><span class="sideNavText d-md-none d-lg-block"> &nbsp; Pegawai</span></p>
                                </div>
                            </a>
                            <a href="/Laporan/Masuk" class="text-decoration-none" style="width: 100%;">
                                <div id="reportNavBtn" class="px-3 my-3 sidebarItem <?php if ($status == "Report") echo "card shadow py-2 bg8" ?>">
                                    <p class="openSans m-0 d-flex align-items-center <?php if ($status == "Report") echo "color4";
                                                                                        else echo "color8" ?>"> <i class="bi bi-journal-text" style="font-size: 25px;"></i><span class="sideNavText d-md-none d-lg-block"> &nbsp; Laporan</span></p>
                                </div>
                            </a>
                            <a href="/logout" class="text-decoration-none" style="width: 100%;">
                                <div class="pl-2 pr-3 mt-3 mb-3 mb-md-2 sidebarItem">
                                    <p class="pl-1 openSans m-0 d-flex align-items-center color5"> <i class="bi bi-box-arrow-left" style="font-size: 27px;"></i><span class="sideNavText d-md-none d-lg-block"> &nbsp; Logout</span></p>
                                </div>
                            </a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <main class="col col-md-11 col-lg p-3 pl-md-3 pl-lg-0">
            <?= $this->include("components/" . $status) ?>
        </main>
    </div>

    <script>
        function showNotification(status, message) {
            $("#notificationMsg").text(message)
            switch (status) {
                case "success":
                    $("#notification").removeClass("bg5")
                    $("#notification").addClass("bg1")
                    $("#notification").show()
                    $("#notification").addClass("d-flex")
                    setTimeout(() => $("#notification").fadeOut(500), 700)
                    setTimeout(() => $("#notification").removeClass("d-flex"), 1200)
                    break;
                case "failed":
                    $("#notification").removeClass("bg1")
                    $("#notification").addClass("bg5")
                    $("#notification").show()
                    $("#notification").addClass("d-flex")
                    setTimeout(() => $("#notification").fadeOut(500), 700)
                    setTimeout(() => $("#notification").removeClass("d-flex"), 1200)
                    break;
            }
        }
        $("#backdrop").click(() => {
            showSideBar(false)
        })

        function showSideBar(status) {
            if (status) {
                $("#backdrop").fadeIn(300)
                $("#sideBarContent").removeClass("d-none")
                if ($("body").width() > 767 && $("body").width() < 992) {
                    $(".sidebarItem").children("p").css("justify-content", "start")
                    $("#sideBarBtn").children("i").removeClass("bi-list")
                    $("#sideBarBtn").children("i").addClass("bi-x")
                    $("#sideBarBtn").removeClass("color1")
                    $("#sideBarBtn").addClass("color5")
                    $("#sideBarBtn").attr("onclick", "showSideBar(false)")
                }
                $("#sideBarContent").addClass("col")
                $(".sideBar").animate({
                    width: "250px"
                }, 300)
                setTimeout(() => $(".sideNavText").removeClass("d-md-none"), 150)
                $("body").css("overflow", "hidden")
            } else {
                $("#backdrop").fadeOut(300)
                if ($("body").width() > 767 && $("body").width() < 992) {
                    $("#sideBarBtn").children("i").removeClass("bi-x")
                    $("#sideBarBtn").children("i").addClass("bi-list")
                    $("#sideBarBtn").removeClass("color5")
                    $("#sideBarBtn").addClass("color1")
                    $("#sideBarBtn").attr("onclick", "showSideBar(true)")
                    $(".sideBar").animate({
                        width: "81px"
                    }, 300)
                    setTimeout(() => $(".sidebarItem").children("p").css("justify-content", "center"), 240)
                } else {
                    $(".sideBar").animate({
                        width: "0px"
                    }, 300)
                }
                setTimeout(() => $(".sideNavText").addClass("d-md-none"), 150)
                setTimeout(() => {
                    $("#sideBarContent").removeClass("col")
                    $("#sideBarContent").addClass("d-none")
                }, 300)
                $("body").css("overflow", "scroll")
            }
        }
    </script>
</body>

</html>