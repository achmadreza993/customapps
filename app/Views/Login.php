<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <!-- Css -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- Font & icon -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@500&family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <title>Document</title>
</head>
<style>
    .DMSans {
        font-family: "DM Sans";
    }

    .openSans {
        font-family: "Open Sans";
    }

    .color1 {
        color: #3B82F6;
    }

    .color2 {
        color: #94A3B8;
    }

    .color3 {
        color: #64748B;
    }

    .color4 {
        color: #FEFEFE;
    }

    .color5 {
        color: #EF4444;
    }

    .bg0 {
        background-color: transparent;
    }

    .bg1 {
        background-color: #3B82F6;
    }

    .bg2 {
        background-color: #FEFEFE;
    }

    .bg3 {
        background-color: #F3F4F6;
    }

    .myBtn {
        border: none;
        border-radius: .25rem;
        outline: none;
    }

    .myBtn:focus {
        outline: none;
    }

    .myCard {
        border: none;
        border-radius: .25rem;
        padding: 2rem;
        outline: none;
    }
</style>

<body style="background-color: #EFF6FF;">
    <div style="width: 100vw;height: 90vh;" class="d-flex justify-content-center align-items-center">
        <div>
            <div class="myCard bg2 pb-4 pt-4" style="width: min(90vw,400px);">
                <h2 class="color1 DMSans mb-1 text-center">Logo</h2>
                <h5 class="mb-3 color2 text-center DMSans">Login</h5>
                <form method="POST" action="/auth/login">
                    <div class="form-group">
                        <label class="openSans color2" style="font-size: 14px;" for="usernameField">Username</label>
                        <input type="text" name="username" class="form-control" id="usernameField">
                    </div>
                    <div class="form-group mb-4">
                        <label class="openSans color2" style="font-size: 14px;" for="passwordField">Password</label>
                        <input type="password" name="password" class="form-control" id="passwordField">
                    </div>
                    <button type="submit" style="width: 100%;" class="myBtn bg1 color4 py-2 mb-2">Submit</button>
                </form>
            </div>
        </div>
    </div>
</body>

</html>