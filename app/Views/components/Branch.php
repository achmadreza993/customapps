<div class="d-flex justify-content-center row">
    <div class="col">
        <div class="card bg2 ml-md-3 shadow">
            <div class="p-3 roundedTop bg10">
                <h4 class="m-0 DMSans color4">Daftar Cabang</h4>
            </div>
            <div class="p-4">
                <div class="d-flex justify-content-between align-items-center m-0">
                    <Button onclick="showSide(this,'insert')" id="insertBtn" class="myBtn color10 openSans p-0 d-flex align-items-center bg2" style="margin-left: -5px;font-size: 14px;"> <i class="bi bi-plus" style="font-size: 25px;"></i> Tambah</Button>
                    <div class="d-flex align-items-center">
                        <Button id="filterBtn1" onclick="toggleFilterGroup()" data-toggle="collapse" data-target="#filterGroup" class="myBtn color4 pl-3 pr-4 d-none mr-2 openSans bg10" style="padding-top: .4rem;padding-bottom: .4rem; font-size: 14px;"> <i class="bi bi-funnel" style="font-size: 18px;"></i> &nbsp; Filter</Button>
                        <Button id="filterBtn0" onclick="toggleFilterGroup()" data-toggle="collapse" data-target="#filterGroup" class="myBtn color10 pl-3 d-flex align-items-center openSans bg2" style="padding-top: .4rem;padding-bottom: .4rem; font-size: 14px;"> <i class="bi bi-funnel" style="font-size: 18px;"></i> &nbsp; Filter</Button>
                    </div>
                </div>
                <div class="collapseGroup accordion" id="accordionExample">
                    <div id="filterGroup" class="collapse" data-parent="#accordionExample">
                        <div class="p-4 myBtn bg3 mt-3">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <p class="mb-1 color2 openSans" style="font-size: 14px;">Search :</p>
                                    <input type="text" class="px-2 py-1 border10" style="width:100%" placeholder="Cari nama cabang...">
                                </div>
                                <div class="col-12 col-md-6 ">
                                    <p class="mb-1 color2 openSans" style="font-size: 14px;">Sort :</p>
                                    <div class="mb-2">
                                        <button id="byName" class="openSans bg10 px-5 py-2 color4 myBtn" style="font-size: 15px;">Nama</button>
                                    </div>
                                    <div>
                                        <button id="byAsc" onclick="sortBy1('Asc')" class="openSans bg10 px-5 py-2 color4 myBtn" style="font-size: 15px;">Asc</button>
                                        <button id="byDesc" onclick="sortBy1('Desc')" class="openSans bg3 py-2 color2 myBtn" style="font-size: 15px;">Desc</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="myTable table table-hover mb-0 mt-3 openSans">
                        <thead class="color3">
                            <tr>
                                <th scope="col" style="width: 60px;">No</th>
                                <th scope="col" style="width: 35%;">Nama</th>
                                <th scope="col" style="width: 35%;">Alamat</th>
                                <th scope="col" style="width: 80px;"></th>
                            </tr>
                        </thead>
                        <tbody class="color2" id="branchTable">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="backdrop1" class="position-fixed" style="display: none;top:0; width: 100vw; height: 100vh;background-color: rgb(17,24,39,.5);"></div>
    <div style="width: 0px; height:auto" id="sideWrapper">
        <div class="mr-3 p-4 position-fixed bg2 myCard shadow" style="width: 24.5%; display: none;" id="sideContent">
            <h5 id="branchSideBarTitle" class="DMSans color10 mb-3">Tambah Cabang</h5>
            <form id="branchForm">
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="nameField">Nama</label>
                    <input type="text" class="form-control openSans" required id="nameField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="addressField">Alamat</label>
                    <input type="text" class="form-control openSans" required id="addressField">
                </div>
                <div class="d-flex justify-content-between">
                    <button type="button" class="myBtn bg4 color4 px-4 py-2 openSans" onclick="showSide(this,'edit')" style="font-size: 15px;">Batal</button>
                    <button type="submit" class="myBtn bg10 color4 px-4 py-2 openSans" style="font-size: 15px;">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    let data;
    let dataAnchor;
    let order = "asc";
    getAllData()

    function getAllData() {
        $("#branchTable").text("")
        $.get(
            `api/cabang?entity=nama&order=${order}`, res => {
                let no = 1;
                data = res;
                res.forEach(d => {
                    $("#branchTable").append(`
                            <tr>
                                <th scope="row">${no++}</th>
                                <td class="text-truncate">${d.nama}</td>
                                <td class="text-truncate">${d.alamat}</td>
                                <td class="px-1 pt-2 pb-1 d-flex align-items-center">
                                    &nbsp;<button class="showSideBtn myBtn bg0" onclick="showSide(this,'edit')"><i class="bi bi-pencil-square color12" style="font-size: 20px;"></i></button>
                                    &nbsp;<button class="deleteBtn myBtn bg0" onclick="showSide(this,'delete')"><i class="bi bi-trash color5" style="font-size: 20px;"></i></button>
                                </td>
                            </tr>`)
                });
            }
        )
    }

    $("#branchForm").submit(e => {
        e.preventDefault()
        console.log($("#branchSideBarTitle").text() == "Tambah Cabang");
        switch ($("#branchSideBarTitle").text()) {
            case "Tambah Cabang":
                $.ajax({
                    type: "POST",
                    url: "/api/cabang",
                    data: '{"nama":"' + $("#nameField").val() + '","alamat":"' + $("#addressField").val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: (res) => {
                        console.log(res);
                    },
                    error: (err) => {
                        console.log(err);
                    }
                });
                showNotification("success", "Data berhasil ditambahkan")
                break;
            case "Edit Cabang":
                for (let i = 0; i < data.length; i++) {
                    if (dataAnchor == data[i].nama) {
                        $.ajax({
                            type: "PUT",
                            url: `/api/cabang/${data[i].id}`,
                            data: '{"nama":"' + $("#nameField").val() + '","alamat":"' + $("#addressField").val() + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: (res) => {
                                console.log(res);
                            },
                            error: (err) => {
                                console.log(err);
                            }
                        });
                        break;
                    }
                }
                showSide(null, "close")
                showNotification("success", "Data berhasil diganti")
                break;
            case "Hapus Cabang":
                for (let i = 0; i < data.length; i++) {
                    if (dataAnchor == data[i].nama) {
                        showSide(null, 'close')
                        $.ajax({
                            type: "DELETE",
                            url: `/api/cabang/${data[i].id}`,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: (res) => {
                                console.log(res);
                            },
                            error: (err) => {
                                console.log(err);
                            }
                        });
                        showNotification("failed", "Data berhasil dihapus")
                        break;
                    }
                }
                break;
        }
        $("#nameField").val("")
        $("#addressField").val("")
        getAllData()
    })

    $("#backdrop1").click(() => {
        showSide(null, "close")
    })

    function toggleFilterGroup() {
        $("#filterBtn1").toggleClass("d-none")
        $("#filterBtn1").toggleClass("d-flex align-items-center")
        $("#filterBtn0").toggleClass("d-flex align-items-center")
        $("#filterBtn0").toggleClass("d-none")
    }

    function sortBy1(status) {
        if (status == 'Asc') {
            $("#byAsc").removeClass("bg3 py-2 color2")
            $("#byAsc").addClass("bg10 px-5 py-2 color4")
            $("#byDesc").removeClass("bg10 px-5 py-2 color4")
            $("#byDesc").addClass("bg3 py-2 color2")
            order = "asc"
        }
        if (status == 'Desc') {
            $("#byDesc").removeClass("bg3 py-2 color2")
            $("#byDesc").addClass("bg10 px-5 py-2 color4")
            $("#byAsc").removeClass("bg10 px-5 py-2 color4")
            $("#byAsc").addClass("bg3 py-2 color2")
            order = "desc"
        }
        getAllData()
    }

    function showSide(e, name) {
        let status;
        switch (name) {
            case "close":
                status = false
                break;
            case "insert":
                status = $(e).hasClass("color10") && $(e).hasClass("bg2")
                break;
            case "edit":
                status = $(e).children("i").hasClass("bi-pencil-square")
                break;
            case "delete":
                status = $(e).children("i").hasClass("bi-trash")
                break;
        }
        $(".showSideBtn").children("i").removeClass("bi-x-square")
        $(".showSideBtn").children("i").addClass("bi-pencil-square")
        $(".showSideBtn").children("i").removeClass("color5")
        $(".showSideBtn").children("i").addClass("color10")

        $(".deleteBtn").children("i").removeClass("bi-x-square")
        $(".deleteBtn").children("i").addClass("bi-trash")
        $(".deleteBtn").children("i").removeClass("color2")
        $(".deleteBtn").children("i").addClass("color5")

        $("#insertBtn").removeClass("color4")
        $("#insertBtn").addClass("color10")
        $("#insertBtn").removeClass("bg10")
        $("#insertBtn").addClass("bg2")
        $("#insertBtn").removeClass("pr-4")
        $("#insertBtn").removeClass("pl-3")
        $("#insertBtn").addClass("p-0")

        switch (name) {
            case "insert":
                $("#branchSideBarTitle").text("Tambah Cabang")
                $("#branchForm").text("")
                $("#branchForm").append(`<div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="nameField">Nama</label>
                    <input type="text" class="form-control openSans" required id="nameField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="addressField">Alamat</label>
                    <input type="text" class="form-control openSans" required id="addressField">
                </div>
                <div class="d-flex justify-content-between">
                    <button type="button" class="myBtn bg4 color4 px-4 py-2 openSans" onclick="showSide(this,'edit')" style="font-size: 15px;">Batal</button>
                    <button type="submit" class="myBtn bg10 color4 px-4 py-2 openSans" style="font-size: 15px;">Submit</button>
                </div>`)
                $("#nameField").val("")
                $("#addressField").val("")
                if (status) {
                    $("#insertBtn").css("margin-left", "0px")
                    $("#insertBtn").removeClass("color10")
                    $("#insertBtn").removeClass("bg2")
                    $("#insertBtn").removeClass("p-0")
                    $("#insertBtn").addClass("color4")
                    $("#insertBtn").addClass("bg10")
                    $("#insertBtn").addClass("pr-4")
                    $("#insertBtn").addClass("pl-3")
                } else {
                    $("#insertBtn").css("margin-left", "-5px")
                }
                break;
            case "edit":
                $("#branchSideBarTitle").text("Edit Cabang")
                $("#branchForm").text("")
                $("#branchForm").append(`<div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="nameField">Nama</label>
                    <input type="text" class="form-control openSans" required id="nameField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="addressField">Alamat</label>
                    <input type="text" class="form-control openSans" required id="addressField">
                </div>
                <div class="d-flex justify-content-between">
                    <button type="button" class="myBtn bg4 color4 px-4 py-2 openSans" onclick="showSide(this,'edit')" style="font-size: 15px;">Batal</button>
                    <button type="submit" class="myBtn bg10 color4 px-4 py-2 openSans" style="font-size: 15px;">Submit</button>
                </div>`)
                if (status) {
                    $(e).children("i").removeClass("bi-pencil-square")
                    $(e).children("i").addClass("bi-x-square")
                    $(e).children("i").removeClass("color10")
                    $(e).children("i").addClass("color5")
                }
                let tempData = $(e).parent("td").parent("tr").children()
                $("#nameField").val(tempData.eq(1).text())
                $("#addressField").val(tempData.eq(2).text())
                dataAnchor = tempData.eq(1).text()
                break;
            case "delete":
                $("#branchSideBarTitle").text("Hapus Cabang")
                dataAnchor = $(e).parent("td").parent("tr").children().eq(1).text()
                $("#branchForm").text("")
                $("#branchForm").append(`
                <p class="color2">Apakah anda yakin untuk menghapus data ${dataAnchor} ?</p>
                <div class="d-flex justify-content-between">
                    <button type="button" class="myBtn bg4 color4 px-4 py-2 openSans" onclick="showSide(this,'delete')" style="font-size: 15px;">Batal</button>
                    <button type="submit" class="myBtn bg5 color4 px-4 py-2 openSans" style="font-size: 15px;">Hapus</button>
                </div>`)
                if (status) {
                    $(e).children("i").removeClass("bi-trash")
                    $(e).children("i").addClass("bi-x-square")
                    $(e).children("i").removeClass("color5")
                    $(e).children("i").addClass("color2")
                }
                break;
        }

        if (status) {
            $("#sideContent").css("display", "block")
            if ($("body").width() > 767 && $("body").width() < 992) {
                $("#backdrop1").fadeIn(300)
                $("#sideWrapper").animate({
                    width: '400px'
                }, 300);
                $("#sideWrapper").children("div").animate({
                    width: "400px"
                }, 300);
            } else if ($("body").width() < 768) {
                $("#backdrop1").fadeIn(300)
                $("#sideWrapper").animate({
                    width: '100vw'
                }, 300);
                $("#sideWrapper").children("div").animate({
                    width: $("body").width() - 52 + "px"
                }, 300);
            } else {
                $("#sideWrapper").animate({
                    width: '30%'
                }, 300);
                $("#sideContent").animate({
                    width: '24.5%'
                }, 300);
            }
        } else {
            if ($("body").width() < 992) {
                $("#backdrop1").fadeOut(300)
            }
            $("#sideWrapper").animate({
                width: '0px'
            }, 300);
            $("#sideContent").animate({
                width: '0px'
            }, 300);
            setTimeout(() => $("#sideContent").css("display", "none"), 300)
        }
    }
</script>