<div class="row m-0">
    <div class="col pr-0">
        <div class="card shadow bg2">
            <div id="titleWrapper" class="bg8 p-3 roundedTop">
                <h4 class="DMSans color4 m-0">Laporan</h4>
            </div>
            <div class="p-4">
                <div class="d-md-flex justify-content-start">
                    <a href="/Laporan/Masuk" class="text-decoration-none openSans">
                        <div class="<?php if ($report == "Masuk") echo "px-5 bg8";
                                    else echo "pr-3" ?> mb-3 myBtn py-1">
                            <p class="openSans m-0 d-flex justify-content-center align-items-center <?php if ($report == "Masuk") echo "pr-2 color4";
                                                                                                    else echo "color8" ?>" style="font-size: 16px;"> <i class="bi bi-download" style="font-size: 18px;"></i> &nbsp; Masuk</p>
                        </div>
                    </a>
                    <a href="/Laporan/Keluar" class="text-decoration-none openSans">
                        <div class="<?php if ($report == "Keluar") echo "px-5 bg6";
                                    else echo "px-3" ?> mb-3 myBtn py-1">
                            <p class="openSans m-0 d-flex justify-content-center align-items-center <?php if ($report == "Keluar") echo "pr-2 color4";
                                                                                                    else echo "color6" ?>" style="font-size: 16px;"> <i class="bi bi-upload" style="font-size: 18px;"></i> &nbsp; Keluar</p>
                        </div>
                    </a>
                    <a href="/Laporan/Riwayat" class="text-decoration-none openSans">
                        <div class="<?php if ($report == "Riwayat") echo "px-5 bg7";
                                    else echo "px-3" ?> mb-3 myBtn py-1">
                            <p class="openSans m-0 d-flex justify-content-center align-items-center <?php if ($report == "Riwayat") echo "pr-2 color4";
                                                                                                    else echo "color7" ?>" style="font-size: 16px;"> <i class="bi bi-clock-history" style="font-size: 18px;"></i> &nbsp; Riwayat</p>
                        </div>
                    </a>
                </div>
                <?= $this->include("components/Report/" . $report) ?>
            </div>
        </div>
    </div>
</div>

<script>
    let reportStatus = "<?= $report ?>"
    switch (reportStatus) {
        case "Masuk":
            console.log("Masuk");
            $("#reportNavBtn").removeClass("bg6")
            $("#reportNavBtn").removeClass("bg7")
            $("#reportNavBtn").addClass("bg8")
            $("#titleWrapper").removeClass("bg6")
            $("#titleWrapper").removeClass("bg7")
            $("#titleWrapper").addClass("bg8")
            break;
        case "Keluar":
            console.log("Keluar");
            $("#reportNavBtn").addClass("bg6")
            $("#reportNavBtn").removeClass("bg7")
            $("#reportNavBtn").removeClass("bg8")
            $("#titleWrapper").addClass("bg6")
            $("#titleWrapper").removeClass("bg7")
            $("#titleWrapper").removeClass("bg8")
            break;
        case "Riwayat":
            console.log("Riwayat");
            $("#reportNavBtn").removeClass("bg6")
            $("#reportNavBtn").addClass("bg7")
            $("#reportNavBtn").removeClass("bg8")
            $("#titleWrapper").removeClass("bg6")
            $("#titleWrapper").addClass("bg7")
            $("#titleWrapper").removeClass("bg8")
            break;
    }
</script>