<div class="d-flex justify-content-center row">
    <div class="col">
        <div class="card bg2 ml-md-3 shadow">
            <div class="p-3 bg11 roundedTop">
                <h4 class="DMSans color4 m-0">Daftar Pegawai</h4>
            </div>
            <div class="p-4">
                <div class="d-flex justify-content-between align-items-center m-0">
                    <Button onclick="showSide(this,'insert')" id="insertBtn" class="myBtn color11 openSans p-0 d-flex align-items-center bg2" style="margin-left: -5px;font-size: 14px;"> <i class="bi bi-plus" style="font-size: 25px;"></i> Tambah</Button>
                    <div class="d-flex align-items-center">
                        <Button id="filterBtn1" onclick="toggleFilterGroup()" data-toggle="collapse" data-target="#filterGroup" class="myBtn color4 pl-3 pr-4 d-none mr-2 openSans bg11" style="padding-top: .4rem;padding-bottom: .4rem; font-size: 14px;"> <i class="bi bi-funnel" style="font-size: 18px;"></i> &nbsp; Filter</Button>
                        <Button id="filterBtn0" onclick="toggleFilterGroup()" data-toggle="collapse" data-target="#filterGroup" class="myBtn color11 pl-3 d-flex align-items-center openSans bg2" style="padding-top: .4rem;padding-bottom: .4rem; font-size: 14px;"> <i class="bi bi-funnel" style="font-size: 18px;"></i> &nbsp; Filter</Button>
                    </div>
                </div>
                <div class="collapseGroup accordion" id="accordionExample">
                    <div id="filterGroup" class="collapse" data-parent="#accordionExample">
                        <div class="p-4 myBtn bg3 mt-3">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <p class="mb-1 color2 openSans" style="font-size: 14px;">Search :</p>
                                    <input type="text" class="px-2 py-1 border11" style="width:100%" placeholder="Cari nama pegawai...">
                                </div>
                                <div class="col-12 col-md-6 ">
                                    <p class="mb-1 color2 openSans" style="font-size: 14px;">Sort :</p>
                                    <div class="mb-2">
                                        <button id="byName" onclick="sortBy('name')" class="openSans bg11 px-5 py-2 color4 myBtn" style="font-size: 15px;">Nama</button>
                                        <button id="byStock" onclick="sortBy('username')" class="openSans bg3 py-2 color2 myBtn" style="font-size: 15px;">Username</button>
                                    </div>
                                    <div>
                                        <button id="byAsc" onclick="sortBy1('Asc')" class="openSans bg11 px-5 py-2 color4 myBtn" style="font-size: 15px;">Asc</button>
                                        <button id="byDesc" onclick="sortBy1('Desc')" class="openSans bg3 py-2 color2 myBtn" style="font-size: 15px;">Desc</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="myTable table table-hover mb-0 mt-3 openSans">
                        <thead class="color3">
                            <tr>
                                <th scope="col" style="width: 60px;">No</th>
                                <th scope="col" style="width: 20%;">Nama</th>
                                <th scope="col" style="width: 25%;">Username</th>
                                <th scope="col" style="width: 20%;">Alamat</th>
                                <th scope="col" style="width: 20%;">Cabang</th>
                                <th scope="col" style="width: 120px;">Role</th>
                                <th scope="col" style="width: 80px;"></th>
                            </tr>
                        </thead>
                        <tbody class="color2" id="employeeTable">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="backdrop1" class="position-fixed" style="display: none;top:0; width: 100vw; height: 100vh;background-color: rgb(17,24,39,.5);"></div>
    <div style="width: 0px; height:auto" id="sideWrapper">
        <div class="mr-3 p-4 position-fixed bg2 myCard shadow employeeSideContent" style="width: 24.5%; display: none;overflow-y: auto;" id="sideContent">
            <h5 id="itemSideBarTitle" class="DMSans color11 mb-3">Tambah Pegawai</h5>
            <form id="employeeForm">
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="nameField">Nama</label>
                    <input type="text" class="form-control openSans" id="nameField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="usernameField">Username</label>
                    <input type="text" class="form-control openSans" id="usernameField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="alamatField">Alamat</label>
                    <input type="text" class="form-control openSans" id="alamatField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="cabangField">Cabang</label>
                    <select class="form-control openSans" id="cabangField"></select>
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="roleField">Role</label>
                    <input type="text" class="form-control openSans" id="roleField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="passField">Password</label>
                    <input type="text" class="form-control openSans" id="passField">
                </div>
                <div class="d-flex justify-content-between">
                    <button type="button" class="myBtn bg4 color4 px-4 py-2 openSans" onclick="showSide(this,'edit')" style="font-size: 15px;">Cancel</button>
                    <button type="submit" class="myBtn bg11 color4 px-4 py-2 openSans" style="font-size: 15px;">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    let data;
    let dataAnchor;
    let entity = "nama";
    let order = "asc";
    getAllData()

    function getAllData() {
        $("#employeeTable").text("")
        $.get(
            `/api/user?entity=${entity}&order=${order}`, res => {
                let no = 1;
                data = res;
                res.forEach(d => {
                    $("#employeeTable").append(`
                            <tr>
                                <th scope="row">${no++}</th>
                                <td class="text-truncate">${d.nama}</td>
                                <td class="text-truncate">${d.username}</td>
                                <td class="text-truncate">${d.alamat}</td>
                                <td class="text-truncate">${d.id_cabang}</td>
                                <td class="text-truncate">${d.role}</td>
                                <td class="px-1 pt-2 pb-1 d-flex align-items-center">
                                    &nbsp;<button class="showSideBtn myBtn bg0" onclick="showSide(this,'edit')"><i class="bi bi-pencil-square color11" style="font-size: 20px;"></i></button>
                                    &nbsp;<button class="deleteBtn myBtn bg0" onclick="showSide(this,'delete')"><i class="bi bi-trash color5" style="font-size: 20px;"></i></button>
                                </td>
                            </tr>`)
                });
            }
        )
    }

    $("#employeeForm").submit(e => {
        e.preventDefault()
        switch ($("#itemSideBarTitle").text()) {
            case "Tambah Pegawai":
                $.ajax({
                    type: "POST",
                    url: "/api/user",
                    data: '{"nama":"' + $("#nameField").val() + '","username":"' + $("#usernameField").val() + '","alamat":"' + $("#alamatField").val() + '","id_cabang":"' + $("#cabangField").val() + '","role":"' + $("#roleField").val() + '","password":"' + $("#passField").val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: (res) => {
                        console.log(res);
                    },
                    error: (err) => {
                        console.log(err);
                    }
                });
                showNotification("success", "Data berhasil ditambahkan")
                break;
            case "Edit Pegawai":
                for (let i = 0; i < data.length; i++) {
                    if (dataAnchor == data[i].nama) {
                        $.ajax({
                            type: "PUT",
                            url: `/api/user/${data[i].id}`,
                            data: '{"nama":"' + $("#nameField").val() + '","username":"' + $("#usernameField").val() + '","alamat":"' + $("#alamatField").val() + '","id_cabang":"' + $("#cabangField").val() + '","role":"' + $("#roleField").val() + '","password":"' + $("#passField").val() + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: (res) => {
                                console.log(res);
                            },
                            error: (err) => {
                                console.log(err);
                            }
                        });
                        break;
                    }
                }
                showSide(null, "close")
                showNotification("success", "Data berhasil diganti")
                break;
            case "Hapus Pegawai":
                for (let i = 0; i < data.length; i++) {
                    if (dataAnchor == data[i].nama) {
                        showSide(null, 'close')
                        $.ajax({
                            type: "DELETE",
                            url: `/api/user/${data[i].id}`,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: (res) => {
                                console.log(res);
                            },
                            error: (err) => {
                                console.log(err);
                            }
                        });

                        showNotification("failed", "Data berhasil dihapus")
                        break;
                    }
                }
                break;
        }
        $("#nameField").val("")
        $("#passField").val("")
        $("#usernameField").val("")
        $("#alamatField").val("")
        $("#cabangField").val("")
        $("#roleField").val("")
        getAllData()
    })

    $("#backdrop1").click(() => {
        showSide(null, "close")
    })

    function toggleFilterGroup() {
        $("#filterBtn1").toggleClass("d-none")
        $("#filterBtn1").toggleClass("d-flex align-items-center")
        $("#filterBtn0").toggleClass("d-flex align-items-center")
        $("#filterBtn0").toggleClass("d-none")
    }

    function sortBy(status) {
        if (status == 'name') {
            $("#byName").removeClass("bg3 py-2 color2")
            $("#byName").addClass("bg11 px-5 py-2 color4")
            $("#byStock").removeClass("bg11 px-5 py-2 color4")
            $("#byStock").addClass("bg3 py-2 color2")
            entity = "nama"
        }
        if (status == 'username') {
            $("#byStock").removeClass("bg3 py-2 color2")
            $("#byStock").addClass("bg11 px-5 py-2 color4")
            $("#byName").removeClass("bg11 px-5 py-2 color4")
            $("#byName").addClass("bg3 py-2 color2")
            entity = "username"
        }
        getAllData()
    }

    function sortBy1(status) {
        if (status == 'Asc') {
            $("#byAsc").removeClass("bg3 py-2 color2")
            $("#byAsc").addClass("bg11 px-5 py-2 color4")
            $("#byDesc").removeClass("bg11 px-5 py-2 color4")
            $("#byDesc").addClass("bg3 py-2 color2")
            order = "asc"
        }
        if (status == 'Desc') {
            $("#byDesc").removeClass("bg3 py-2 color2")
            $("#byDesc").addClass("bg11 px-5 py-2 color4")
            $("#byAsc").removeClass("bg11 px-5 py-2 color4")
            $("#byAsc").addClass("bg3 py-2 color2")
            order = "desc"
        }
        getAllData()
    }

    function showSide(e, name) {
        let status;
        switch (name) {
            case "close":
                status = false
                break;
            case "insert":
                status = $(e).hasClass("color11") && $(e).hasClass("bg2")
                break;
            case "edit":
                status = $(e).children("i").hasClass("bi-pencil-square")
                break;
            case "delete":
                status = $(e).children("i").hasClass("bi-trash")
                break;
        }
        $(".showSideBtn").children("i").removeClass("bi-x-square")
        $(".showSideBtn").children("i").addClass("bi-pencil-square")
        $(".showSideBtn").children("i").removeClass("color5")
        $(".showSideBtn").children("i").addClass("color11")

        $(".deleteBtn").children("i").removeClass("bi-x-square")
        $(".deleteBtn").children("i").addClass("bi-trash")
        $(".deleteBtn").children("i").removeClass("color2")
        $(".deleteBtn").children("i").addClass("color5")

        $("#insertBtn").removeClass("color4")
        $("#insertBtn").addClass("color11")
        $("#insertBtn").removeClass("bg11")
        $("#insertBtn").addClass("bg2")
        $("#insertBtn").removeClass("pr-4")
        $("#insertBtn").removeClass("pl-3")
        $("#insertBtn").addClass("p-0")

        switch (name) {
            case "insert":
                $("#itemSideBarTitle").text("Tambah Pegawai")
                $("#employeeForm").text("")
                $("#employeeForm").append(`<div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="nameField">Nama</label>
                    <input type="text" class="form-control openSans" id="nameField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="usernameField">Username</label>
                    <input type="text" class="form-control openSans" id="usernameField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="alamatField">Alamat</label>
                    <input type="text" class="form-control openSans" id="alamatField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="cabangField">Cabang</label>
                    <select class="form-control openSans" id="cabangField"></select>
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="roleField">Role</label>
                    <input type="text" class="form-control openSans" id="roleField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="passField">Password</label>
                    <input type="text" class="form-control openSans" id="passField">
                </div>
                <div class="d-flex justify-content-between">
                    <button type="button" class="myBtn bg4 color4 px-4 py-2 openSans" onclick="showSide(this,'edit')" style="font-size: 15px;">Cancel</button>
                    <button type="submit" class="myBtn bg11 color4 px-4 py-2 openSans" style="font-size: 15px;">Submit</button>
                </div>`)
                $("#nameField").val("")
                $("#passField").val("")
                $("#usernameField").val("")
                $("#alamatField").val("")
                $("#cabangField").val("")
                $("#roleField").val("")
                $.get(
                    `/api/cabang?entity=${entity}&order=${order}`, res => {
                        res.forEach(d => {
                            $("#cabangField").append(`<option value="${d.id}" >${d.nama}</option>`)
                        });
                    }
                )
                if (status) {
                    $("#insertBtn").css("margin-left", "0px")
                    $("#insertBtn").removeClass("color11")
                    $("#insertBtn").removeClass("bg2")
                    $("#insertBtn").removeClass("p-0")
                    $("#insertBtn").addClass("color4")
                    $("#insertBtn").addClass("bg11")
                    $("#insertBtn").addClass("pr-4")
                    $("#insertBtn").addClass("pl-3")
                } else {
                    $("#insertBtn").css("margin-left", "-5px")
                }
                break;
            case "edit":
                $("#itemSideBarTitle").text("Edit Pegawai")
                $("#employeeForm").text("")
                $("#employeeForm").append(`<div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="nameField">Nama</label>
                    <input type="text" class="form-control openSans" id="nameField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="usernameField">Username</label>
                    <input type="text" class="form-control openSans" id="usernameField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="alamatField">Alamat</label>
                    <input type="text" class="form-control openSans" id="alamatField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="cabangField">Cabang</label>
                    <select class="form-control openSans" id="cabangField"></select>
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="roleField">Role</label>
                    <input type="text" class="form-control openSans" id="roleField">
                </div>
                <div class="form-group">
                    <label class="color2 openSans" style="font-size: 14px;" for="passField">Password</label>
                    <input type="text" class="form-control openSans" id="passField">
                </div>
                <div class="d-flex justify-content-between">
                    <button type="button" class="myBtn bg4 color4 px-4 py-2 openSans" onclick="showSide(this,'edit')" style="font-size: 15px;">Cancel</button>
                    <button type="submit" class="myBtn bg11 color4 px-4 py-2 openSans" style="font-size: 15px;">Submit</button>
                </div>`)
                if (status) {
                    $(e).children("i").removeClass("bi-pencil-square")
                    $(e).children("i").addClass("bi-x-square")
                    $(e).children("i").removeClass("color11")
                    $(e).children("i").addClass("color5")
                }
                let tempData = $(e).parent("td").parent("tr").children()
                $("#nameField").val(tempData.eq(1).text())
                $("#usernameField").val(tempData.eq(2).text())
                $("#alamatField").val(tempData.eq(3).text())
                $("#roleField").val(tempData.eq(5).text())
                $.get(
                    `/api/cabang?entity=${entity}&order=${order}`, res => {
                        res.forEach(d => {
                            if (d.id == tempData.eq(4).text()) {
                                $("#cabangField").append(`<option value="${d.id}" selected>${d.nama}</option>`)
                            } else {
                                $("#cabangField").append(`<option value="${d.id}" >${d.nama}</option>`)
                            }
                        });
                    }
                )
                dataAnchor = tempData.eq(1).text()
                break;
            case "delete":
                $("#itemSideBarTitle").text("Hapus Pegawai")
                dataAnchor = $(e).parent("td").parent("tr").children().eq(1).text()
                $("#employeeForm").text("")
                $("#employeeForm").append(`
                <p class="color2">Apakah anda yakin untuk menghapus data ${dataAnchor} ?</p>
                <div class="d-flex justify-content-between">
                    <button type="button" class="myBtn bg4 color4 px-4 py-2 openSans" onclick="showSide(this,'delete')" style="font-size: 15px;">Batal</button>
                    <button type="submit" class="myBtn bg5 color4 px-4 py-2 openSans" style="font-size: 15px;">Hapus</button>
                </div>`)
                if (status) {
                    $(e).children("i").removeClass("bi-trash")
                    $(e).children("i").addClass("bi-x-square")
                    $(e).children("i").removeClass("color5")
                    $(e).children("i").addClass("color2")
                }
                break;
        }

        if (status) {
            $("#sideContent").css("display", "block")
            if ($("body").width() > 767 && $("body").width() < 992) {
                $("#backdrop1").fadeIn(300)
                $("#sideWrapper").animate({
                    width: '400px'
                }, 300);
                $("#sideWrapper").children("div").animate({
                    width: "400px"
                }, 300);
            } else if ($("body").width() < 768) {
                $("#backdrop1").fadeIn(300)
                $("#sideWrapper").animate({
                    width: '100vw'
                }, 300);
                $("#sideWrapper").children("div").animate({
                    width: $("body").width() - 52 + "px"
                }, 300);
            } else {
                $("#sideWrapper").animate({
                    width: '30%'
                }, 300);
                $("#sideContent").animate({
                    width: '24.5%'
                }, 300);
            }
        } else {
            if ($("body").width() < 992) {
                $("#backdrop1").fadeOut(300)
            }
            $("#sideWrapper").animate({
                width: '0px'
            }, 300);
            $("#sideContent").animate({
                width: '0px'
            }, 300);
            setTimeout(() => $("#sideContent").css("display", "none"), 300)
        }
    }
</script>