<div class="row m-0 pl-md-3 todayInformation">
    <div class="px-0  pb-3 card mb-3 bg2 shadow px-0 col-12 col-sm-12 col-md-12 col-lg-6 col-xl-5">
        <div class="row-cols-1">
            <div class="p-3 mb-3 bg1 px-3 pb-0 roundedTop">
                <h5 class="card-title DMSans m-0 color4">Informasi Singkat</h5>
            </div>
            <div class="col px-3 pb-3">
                <button class="todayInfoNav myBtn openSans px-4 color4 py-2 bg1">Hari ini</button>
                <button class="todayInfoNav myBtn openSans py-2 color2 bg0">Minggu ini</button>
                <button class="todayInfoNav myBtn openSans py-2 color2 bg0">Bulan ini</button>
            </div>
            <div class="pb-3 px-3 col">
                <a href="/Laporan/Masuk" class="text-decoration-none">
                    <div class="card bg8 p-0">
                        <div class="pt-3 px-3 pb-0 roundedTop">
                            <p class="card-title DMSans m-0 color4">Jumlah Barang Masuk</p>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="px-3 pt-1">
                                <h1 class="align-self-center card-text openSans m-0 color4">100</h1>
                            </div>
                            <i class="align-self-center mb-1 bi bi-download mr-2 color4 pr-3" style="font-size: 40px;"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="px-3 col">
                <a href="/Laporan/Keluar" class="text-decoration-none">
                    <div class="card bg6">
                        <div class="roundedTop pt-3 px-3">
                            <p class="card-title DMSans m-0 color4">Jumlah Barang Keluar</p>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="px-3 pt-1">
                                <h1 class="card-text openSans m-0 color4">100</h1>
                            </div>
                            <i class="align-self-center pb-1 pr-3 bi bi-upload mr-2 color4" style="font-size: 40px;"></i>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="pb-3 px-0 col-12 pl-lg-3 col-sm-12 col-md-12 col-lg-6 col-xl-7">
        <div class="card shadow" style="height: 100%;">
            <a href="/Laporan/Riwayat" class="text-decoration-none">
                <div class="roundedTop bg7 d-flex justify-content-between">
                    <h5 class="p-3 DMSans m-0 color4">Riwayat Terakhir</h5>
                    <i class="bi bi-clock-history color4 align-self-center mr-3" style="font-size: 30px;"></i>
                </div>
            </a>
            <div class="p-3">
                <div id="reasonWrapper">
                    <div class="py-3 px-4 rounded" style="background-color: #34D399;height: 100%;">
                        <div class="rounded" id="usernameIpWrapper">
                            <p class="openSans bg3 py-1 px-3 rounded" style="color: #34D399;">Username</p>
                            <p class="m-0 openSans bg3 py-1 px-3 rounded" style="color: #34D399;">Ip Address</p>
                        </div>
                        <div class="my-3 mt-1" style="overflow-y: auto;height: 168px;">
                            <h5 class="card-text openSans mb-0 color4 pr-1">Lorem ipsum dolor sit Lorem ipsum, dolor sit amet consectetur adipisicing elit. Natus expedita nam accusantium debitis explicabo asperiores voluptas alias non. Aliquid tenetur voluptatem voluptas. Praesentium laudantium quasi, perferendis deserunt cumque asperiores officiis! amet consectetur adipisicing elit. Reiciendis molestias nostrum facilis et nobis, ipsa consequuntur quia, iusto sint eum laboriosam aspernatur blanditiis. Neque vero amet ratione repellendus voluptates soluta.</h5>
                        </div>
                        <p class="color4 openSans float-right align-items-end">20:20</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row m-0">
    <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 p-0 itemList">
        <div class="card ml-md-3 my-0 shadow">
            <a href="/Barang" class="p-3 roundedTop bg9 text-decoration-none">
                <h5 class="DMSans color4 m-0">Daftar Barang</h5>
            </a>
            <div class="px-4 pt-4">
                <div class="table-responsive ">
                    <table class="table table-hover openSans myTable">
                        <thead class="color3">
                            <tr>
                                <th scope="col" style="width: 60px;">No</th>
                                <th scope="col" style="width: 30%;">Nama</th>
                                <th scope="col" style="width: 20%;">Stok</th>
                                <th scope="col" style="width: 20%;">Cabang</th>
                                <th scope="col" style="width: 20%;">File</th>
                            </tr>
                        </thead>
                        <tbody id="homeItemTable" class="color2">
                        </tbody>
                    </table>
                </div>
            </div>
            <a href="/Barang" class="color9 pb-1 pt-4 pt-md-0 myBtn text-decoration-none text-center" style="margin-top: -30px;">
                <i class="bi bi-chevron-compact-down" style="font-size: 25px;"></i>
            </a>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 p-0 branchList">
        <div class="card ml-md-3 mt-3 mt-lg-0 shadow">
            <a href="/Cabang" class="bg10 p-3 roundedTop text-decoration-none">
                <h5 class="DMSans color4 m-0">Daftar Cabang</h5>
            </a>
            <div class="px-4 pt-4">
                <div class="table-responsive ">
                    <table class="table table-hover openSans myTable">
                        <thead class="color3">
                            <tr>
                                <th scope="col" style="width: 60px;">No</th>
                                <th scope="col">Nama</th>
                            </tr>
                        </thead>
                        <tbody class="color2" id="homeBranchTable">
                        </tbody>
                    </table>
                </div>
            </div>
            <a href="/Cabang" class="color10 pb-1 myBtn text-decoration-none text-center" style="margin-top: -30px;">
                <i class="bi bi-chevron-compact-down" style="font-size: 25px;"></i>
            </a>
        </div>
    </div>
</div>
<div class="row m-0">
    <div class="col p-0 employeeList">
        <div class="card ml-md-3 mt-3 shadow">
            <a href="/Pegawai" class="bg11 roundedTop text-decoration-none p-3">
                <h5 class="DMSans color4 m-0">Daftar Pegawai</h5>
            </a>
            <div class="px-4 pt-4">
                <div class="table-responsive">
                    <table class="table table-hover mb-0 openSans myTable">
                        <thead class="color3">
                            <tr>
                                <th scope="col" style="width: 60px;">No</th>
                                <th scope="col" style="width: 20%;">Nama</th>
                                <th scope="col" style="width: 15%;">Username</th>
                                <th scope="col" style="width: 20%;">Alamat</th>
                                <th scope="col" style="width: 20%;">Cabang</th>
                                <th scope="col" style="width: 90px;">Role</th>
                            </tr>
                        </thead>
                        <tbody class="color2" id="homeEmployeeTable">

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <a href="/Pegawai" class="color11 pt-4 pt-md-0 myBtn text-decoration-none text-center pb-1 pt-2" style="margin-top: -10px;">
                <i class="bi bi-chevron-compact-down" style="font-size: 25px;"></i>
            </a>
        </div>
    </div>
</div>

<script>
    $.get(
        "/api/barang?entity=nama&order=asc", res => {
            let no = 1;
            res.slice(0, 3).forEach(d => {
                $("#homeItemTable").append(`
                <tr>
                    <td class="text-truncate">${no++}</td>
                    <td class="text-truncate">${d.nama}</td>
                    <td class="text-truncate">${d.stok}</td>
                    <td class="text-truncate">${d.id_cabang}</td>
                    <td class="text-truncate">${d.file}</td>
                </tr>`)
            });
        })
    $.get(
        "/api/cabang?entity=nama&order=asc", res => {
            let no = 1;
            res.slice(0, 3).forEach(d => {
                $("#homeBranchTable").append(`
                <tr>
                    <td class="text-truncate">${no++}</td>
                    <td class="text-truncate">${d.nama}</td>
                </tr>`)
            });
        }
    )
    $.get(
        "/api/user?entity=nama&order=asc", res => {
            let no = 1;
            res.slice(0, 3).forEach(d => {
                $("#homeEmployeeTable").append(`
                <tr>
                    <td class="text-truncate">${no++}</td>
                    <td class="text-truncate">${d.nama}</td>
                    <td class="text-truncate">${d.username}</td>
                    <td class="text-truncate">${d.alamat}</td>
                    <td class="text-truncate">${d.id_cabang}</td>
                    <td class="text-truncate">${d.role}</td>
                </tr>`)
            });
        }
    )
</script>