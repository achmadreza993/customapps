<?php

namespace App\Controllers;

class View extends BaseController
{
    public function viewManager($name, $report = "")
    {
        return view("Main", ["status" => $name, "report" => $report]);
    }
    public function index()
    {
        return $this->viewManager("Home");
    }
    public function barang()
    {
        return $this->viewManager("Item");
    }
    public function cabang()
    {
        return $this->viewManager("Branch");
    }
    public function pegawai()
    {
        return $this->viewManager("Employee");
    }
    public function laporan($status)
    {
        return $this->viewManager("Report", $status);
    }

    public function login()
    {
        return view("Login");
    }
}
