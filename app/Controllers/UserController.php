<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\RESTful\ResourceController;

use function PHPUnit\Framework\isEmpty;

class UserController extends ResourceController
{
	protected $format = 'json';
	protected $modelName = 'App\Models\User';
	public function index()
	{
		$entity = $this->request->getVar('entity');
		$order = $this->request->getVar('order');
		return $this->respond($this->model->cabang()->findAll(), 200);
	}
	public function show($id = null)
	{
		return $this->respond($this->model->cabang()->find($id), 200);
	}
	public function create()
	{
		$data = [
			'id' => BaseController::randId(),
			'nama' => $this->request->getJsonVar('nama'),
			'username' => $this->request->getJsonVar('username'),
			'password' => $this->request->getJsonVar('password'),
			'alamat' => $this->request->getJsonVar('alamat'),
			'id_cabang' => $this->request->getJsonVar('id_cabang'),
			'role' => $this->request->getJsonVar('role'),
		];
		// return $data;
		return $this->model->insert($data);
	}
	public function update($id = null)
	{
		$data = [
			'nama' => $this->request->getJsonVar('nama'),
			'username' => $this->request->getJsonVar('username'),
			'password' => $this->request->getJsonVar('password'),
			'alamat' => $this->request->getJsonVar('alamat'),
			'id_cabang' => $this->request->getJsonVar('id_cabang'),
			'role' => $this->request->getJsonVar('role'),
		];
		return $this->model->update($id, $data);
	}
	public function delete($id = null)
	{
		return $this->model->delete($id);
	}
	public function getUsers()
	{
		$role = $this->request->getVar('role');
		if (isEmpty($role)) {
			return $this->respond($this->model->cabang()->findAll());
		}
		return $this->respond($this->model->cabang()->where('role', $role)->findAll());
	}
}
