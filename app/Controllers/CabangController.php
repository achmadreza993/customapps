<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\Request;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\I18n\Time;
use CodeIgniter\RESTful\ResourceController;

class CabangController extends ResourceController
{
	protected $format = 'json';
	protected $modelName = 'App\Models\Cabang';
	public function index()
	{
		$entity = $this->request->getVar('entity');
		$order = $this->request->getVar('order');
		return $this->respond($this->model->orderBy($entity, $order)->findAll(), 200);
	}
	public function show($id = null)
	{
		return $this->respond($this->model->find($id), 200);
	}
	public function create()
	{
		$data = [
			'id' => BaseController::randId(),
			'nama' => $this->request->getJsonVar('nama'),
			'alamat' => $this->request->getJsonVar('alamat'),
		];
		return $this->model->insert($data);
	}
	public function update($id = null)
	{
		$data = [
			'nama' => $this->request->getJsonVar('nama'),
			'alamat' => $this->request->getJsonVar('alamat'),
		];
		return $this->model->update($id, $data);
	}
	public function delete($id = null)
	{
		return $this->model->delete($id);
	}
}
