<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\User;

class AuthController extends BaseController
{
	protected $model, $session;
	public function __construct()
	{
		$this->model = new User();
		$this->session = session();
	}
	public function loginAttempt()
	{
		$username = $this->request->getPost('username');
		$password = $this->request->getPost('password');

		$cred1 = $this->model->where(['username' => $username])->first();
		/*
		------------------------------------------------------------------
			check apakah user ada? jika ada, verifikasi password. 
			jika tidak keduanya, kembali ke halaman login.
		------------------------------------------------------------------
		*/
		if ($cred1) {
			$verifPassword = password_verify($password, $cred1['password']);
			if ($verifPassword) {
				$userData = [
					'user_id' => $cred1['id'],
					'user_cabang_id' => $cred1['id_cabang'],
					'user_username' => $cred1['username'],
					'user_role' => $cred1['role'],
					'user_logged_in' => true,
				];

				$this->session->set($userData);
				return redirect()->back();
			} else {
				$message = "Password Salah!";
			}
		}
		return redirect()->to('/login');
	}

	public function register()
	{
		$data = [
			'id' => BaseController::randId(),
			'nama' => $this->request->getPost('nama'),
			'id_cabang' => $this->request->getPost('id_cabang'),
			'password' => password_hash($this->request->getPost('password'), PASSWORD_DEFAULT),
			'alamat' => $this->request->getPost('alamat'),
		];
		return $this->model->insert($data);
	}

	public function logout()
	{
		$this->session->destroy();
		return redirect()->to("/login");
	}
}
