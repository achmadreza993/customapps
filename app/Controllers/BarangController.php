<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\RESTful\ResourceController;

class BarangController extends ResourceController
{
	protected $format = 'json';
	protected $modelName = 'App\Models\Barang';		
	public function index()
	{
		$entity = $this->request->getVar('entity');
		$order = $this->request->getVar('order');
		return $this->respond($this->model->cabang()->orderBy($entity, $order)->findAll(),200);
	}
	public function show($id = null)
	{
		return $this->respond($this->model->cabang()->find($id),200);
	}
	public function create()
	{
		$data = [
			'id' => BaseController::randId(),
			'nama' => $this->request->getJsonVar('nama'),
			'stok' => $this->request->getJsonVar('stok'),
			'file' => $this->request->getFile('file'),
			'alamat' => $this->request->getJsonVar('alamat'),
			'tinggi' => $this->request->getJsonVar('tinggi'),
			'id_cabang' => $this->request->getJsonVar('id_cabang'),
			'berat_badan' => $this->request->getJsonVar('berat_badan'),
			'tempat_lahir' => $this->request->getJsonVar('tempat_lahir'),
		];
		return $this->model->insert($data);
	}
	public function update($id = null)
	{
		$data = [
			'nama' => $this->request->getJsonVar('nama'),
			'stok' => $this->request->getJsonVar('stok'),
			'file' => $this->request->getJsonVar('file'),
			'alamat' => $this->request->getJsonVar('alamat'),
			'tinggi' => $this->request->getJsonVar('tinggi'),
			'id_cabang' => $this->request->getJsonVar('id_cabang'),
			'berat_badan' => $this->request->getJsonVar('berat_badan'),
			'tempat_lahir' => $this->request->getJsonVar('tempat_lahir'),
		];
		return $this->model->update($id,$data);
	}
	public function delete($id = null)
	{
		return $this->model->delete($id);	
	}
}
