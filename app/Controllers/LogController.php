<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\RESTful\ResourceController;

class LogController extends ResourceController
{
	protected $format = 'json';
	protected $modelName = 'App\Models\Log';		
	public function index()
	{
		return $this->respond($this->model->findAll(),200);
	}
	public function show($id = null)
	{
		return $this->respond($this->model->find($id),200);
	}
	public function create($id = null)
	{
		$data = [
			'id' => BaseController::randId(),
			'keterangan' => $this->request->getJsonVar('keterangan'),
			'ipaddress' => $this->request->getIPAddress(),
			'user_id' => $id,
		];
		return $this->model->insert($data);
	}
	public function delete($id = null)
	{
		return $this->model->delete($id);	
	}
}
