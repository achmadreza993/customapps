<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->group('', ['filter' => 'alreadylogin'], function ($routes) {
	$routes->get('/login', 'View::login');
});
// $routes->group('', ['filter' => 'auth'], function ($routes) {
	$routes->get('/', 'View::index');
	$routes->get('/Barang', 'View::barang');
	$routes->get('/Pegawai', 'View::pegawai');
	$routes->get('/Cabang', 'View::cabang');
	$routes->get('/Laporan/(:any)', 'View::laporan/$1');
	$routes->addRedirect('/Laporan', 'Laporan/Masuk');
// });
$routes->group('api', ['namespace' => 'App\Controllers'], function ($routes) {
	$routes->resource('barang', ['controller' => 'BarangController']);
	$routes->resource('cabang', ['controller' => 'CabangController']);
	$routes->resource('log', ['controller' => 'LogController']);
	$routes->resource('user', ['controller' => 'UserController']);
	$routes->post('write/log/(:alphanum)', 'LogController::create/$1');
});
$routes->get('users','UserController::getUsers');
// $routes->group('auth', ['namespace' => 'App\Controllers','filter' => 'alreadylogin'], function ($routes) {
	$routes->post('login', 'AuthController::loginAttempt');
	$routes->post('register', 'AuthController::register');
	$routes->get('logout', 'AuthController::logout');
// });
$routes->get('logout', 'AuthController::logout');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
