<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

use function PHPSTORM_META\type;

class Cabang extends Migration
{ 
	public function up()
	{
		$this->forge->addField([
			"id" => [
				'type' => 'VARCHAR',
				'constraint' => '15',
				'auto_increment' => true
			],
			"nama" => [
				'type'=> 'VARCHAR',
				'constraint' => '100',
			],
			"alamat" => [
				'type'=> 'TEXT',
			],
			"created_at" => [
				'type' => 'DATETIME',
				'null' => true
			],
			'updated_at' => [
				'type' => 'DATETIME',
				'null' => true	
			],
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('cabang');
	}

	public function down()
	{
		$this->forge->dropTable('cabang');
	}
}
