<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Log extends Migration
{
	public function up()
	{
		$this->forge->addField([
			"id" => [
				'type' => 'VARCHAR',
				'constraint' => '15',
				'auto_increment' => true
			],
			"keterangan" => [
				'type'=> 'TEXT',
			],
			"user_id" => [
				'type'=> 'VARCHAR',
				'constraint' => 30,
			],
			"ipaddress" => [
				'type'=> 'VARCHAR',
				'constraint' => 30,
			],
			"created_at" => [
				'type' => 'DATETIME',
				'null' => true
			],
			'updated_at' => [
				'type' => 'DATETIME',
				'null' => true	
			],
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('logs');
	}

	public function down()
	{
		$this->forge->dropTable('logs');
	}
}
