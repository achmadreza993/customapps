<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Barang extends Migration
{
	public function up()
	{
		$this->forge->addField([
			"id" => [
				'type' => 'VARCHAR',
				'constraint' => '15',
				'auto_increment' => true
			],
			"nama" => [
				'type'=> 'VARCHAR',
				'constraint' => '100',
			],
			"alamat" => [
				'type'=> 'TEXT',
				'constraint' => '100',
			],
			"tempat_lahir" => [
				'type'=> 'VARCHAR',
				'constraint' => '100',
			],
			"berat_badan" => [
				'type'=> 'INT',
				'constraint' => '100',
			],
			"tinggi" => [
				'type'=> 'INT',
				'constraint' => '100',
			],
			"id_cabang" => [
				'type' => 'VARCHAR',
				'constraint' => '100'
			],
			"file" => [
				'type' => 'BLOB',
			],
			"created_at" => [
				'type' => 'DATETIME',
				'null' => true
			],
			'updated_at' => [
				'type' => 'DATETIME',
				'null' => true	
			],
		]);
		
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('barang');
	}

	public function down()
	{
		$this->forge->dropTable('barang');
	}
}
