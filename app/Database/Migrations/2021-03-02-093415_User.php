<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class User extends Migration
{
	public function up()
	{
		$this->forge->addField([
			"id" => [
				'type' => 'VARCHAR',
				'constraint' => '15',
				'auto_increment' => true
			],
			"nama" => [
				'type'=> 'VARCHAR',
				'constraint' => '100',
			],
			"username" => [
				'type' => 'VARCHAR',
				'constraint' => '100'
			],
			"password" => [
				'type' => 'VARCHAR',
				'constraint' => '100'
			],
			"alamat" => [
				'type' => 'text'
			],
			"id_cabang" => [
				'type' => 'VARCHAR',
				'constraint' => '100',
			],
			"role" => [
				'type' => 'VARCHAR',
				'constraint' => '10'
			],
			"created_at" => [
				'type' => 'DATETIME',
				'null' => true
			],
			'updated_at' => [
				'type' => 'DATETIME',
				'null' => true	
			],
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('users');
	}

	public function down()
	{
		$this->forge->dropTable('users');
	}
}
