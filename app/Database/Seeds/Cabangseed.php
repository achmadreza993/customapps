<?php

namespace App\Database\Seeds;

use App\Controllers\BaseController;
use App\Models\Cabang;
use CodeIgniter\Database\Seeder;

class Cabangseed extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();
		$cabang = new Cabang();
		for ($i = 0; $i < 10; $i++) {
			$data = [
				'id' => BaseController::randId(),
				'nama'        => $faker->name,
				'alamat'        => $faker->address,
				'id_cabang'     => BaseController::randId(),
			];

			$cabang->insert($data);
		}
	}
}
