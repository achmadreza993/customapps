<?php

namespace App\Database\Seeds;

use App\Controllers\BaseController;
use App\Models\Barang;
use CodeIgniter\Database\Seeder;

class BarangSeed extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();
		$barang = new Barang();
		for ($i = 0; $i < 10; $i++) {
			$data = [
				'id' => BaseController::randId(),
				'id_cabang' => BaseController::randId(),
				'stok' => random_int(40,99),
				'nama' => $faker->name,
			];

			$barang->insert($data);
		}
	}
}
