<?php

namespace App\Database\Seeds;

use App\Controllers\BaseController;
use App\Models\User;
use CodeIgniter\Database\Seeder;

class Userseed extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();
		$user = new User();
		for ($i = 0; $i < 10; $i++) {
			$data = [
				'id' => BaseController::randId(),
				'nama' => $faker->name,
				'username' => $faker->userName,
				'password' => password_hash('password',PASSWORD_DEFAULT),
				'alamat' => $faker->streetAddress,
				'id_cabang' => BaseController::randId(),
				'role' => $faker->randomElement(['Admin','Master'])
			];

			$user->insert($data);
		}
	}
}
